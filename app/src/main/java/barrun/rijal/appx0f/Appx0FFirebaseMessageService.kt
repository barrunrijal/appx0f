package barrun.rijal.appx0f

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class Appx0FFirebaseMessageService : FirebaseMessagingService() {

    var promoId = ""
    var promo = ""
    var promoUntil = ""
    var body = ""
    var title = ""
    val RC_Intent = 100
    val Channel_id = "appx0f"

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)
    }

    override fun onMessageReceived(p0: RemoteMessage?) {
        super.onMessageReceived(p0)
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        //Jika ada pesan membawa data
        if (p0?.data?.size!! > 0) {
            promoId = p0.data.getValue("promoId")
            promo = p0.data.getValue("promo")
            promoUntil = p0.data.getValue("promoUntil")

            intent.putExtra("promoId", promoId)
            intent.putExtra("promo", promo)
            intent.putExtra("promoUntil", promoUntil)
            intent.putExtra("type", 0)

            sendNotif("Today Promo !!!", "$promo $promoId", intent)
        }

        //Notifikasi
        if (p0.notification != null) {
            body = p0.notification!!.body!!
            title = p0.notification!!.title!!

            intent.putExtra("title", title)
            intent.putExtra("body", body)
            intent.putExtra("type", 1)

            sendNotif(title, body, intent)
        }
    }

    //Function yang akan dipanggil jika ada pesan masuk
    fun sendNotif(title: String, body: String, intent: Intent) {

        //Pending  intent
        val pendingIntent = PendingIntent.getActivities(
            this, RC_Intent,
            arrayOf(intent), PendingIntent.FLAG_ONE_SHOT
        )

        //Ringtone
        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        val audioAttributes =
            AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()

        //Create notification manager
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        //Target Android 8 (Api Level 26)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(
                Channel_id, "appx0f",
                NotificationManager.IMPORTANCE_HIGH
            )
            mChannel.description = "app x0f"
            mChannel.setSound(ringtoneUri, audioAttributes)
            notificationManager.createNotificationChannel(mChannel)
        }

        val notifcationBuilder = NotificationCompat.Builder(baseContext,Channel_id)
            .setSmallIcon(R.drawable.logo1)
            .setLargeIcon(BitmapFactory.decodeResource(resources,R.drawable.logo2))
            .setContentTitle(title)
            .setContentText(body)
            .setSound(ringtoneUri)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true).build()
        notificationManager.notify(RC_Intent,notifcationBuilder)

    }
}